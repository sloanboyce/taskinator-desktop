# taskinator-desktop
A ported version of Taskinator, a course work project for my bootcamp, just for desktop using Electron.
Disclaimer: Most of the code here was provided in the modules/course work, and I cannot take credit for all of the code that is in this repository. 

## Install/Usage: 
* Download or Clone the files
* If you do not have npm, download it or else the program will not boot
* Once in the file write `npm start`
* Enjoy Taskinator on your desktop :) 
